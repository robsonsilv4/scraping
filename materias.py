import json
import os

import requests
from bs4 import BeautifulSoup


def get_soup(url):
    # Tchau badbot!
    headers = requests.utils.default_headers()
    headers.update(
        {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', })

    r = requests.get(url, headers=headers)
    soup = BeautifulSoup(r.content, 'html.parser')

    return soup


def get_pdf(url, pasta='materias'):
    headers = requests.utils.default_headers()
    headers.update(
        {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', })

    if 'https' not in url:
        url = 'https://www.camarajaguaribara.ce.gov.br/' + url

    r = requests.get(url, headers=headers)
    nome = url.split('/')[-1]

    pastas = f'pdfs/{pasta}/'
    os.makedirs(pastas, exist_ok=True)
    with open(pastas + nome, 'wb') as f:
        f.write(r.content)

    return os.getcwd() + f'/{pastas}{nome}'


def write_json(lista, nome):
    os.makedirs('jsons', exist_ok=True)
    with open(f'jsons/{nome}.json', 'w') as f:
        json.dump(lista, f, ensure_ascii=False, indent=2)

# TODO:
# Os últimos links mudam
# pegar da postagem mesmo
# descrição está vindo vazia


def get_detalhes(url):
    url_base = 'https://www.camarajaguaribara.ce.gov.br/'
    url_detalhes = url_base + url

    soup = get_soup(url_detalhes)
    conteudo = soup.find('div', {'id': 'Noticia'})

    h4 = conteudo.find_all('h4')
    titulo = h4[0].get_text().strip()
    tipo = h4[2].get_text().strip()

    descricao = conteudo.find_all('div')[3].get_text().strip()
    print(descricao)
    # try:
    #     url_pdf = conteudo.find_all('a', {'class': 'lici'})[0].attrs['href']
    #     pdf = get_pdf(url_base + url_pdf)
    # except IndexError:
    #     pdf = ''  # sem pdf anexado

    detalhes = {
        'titulo': titulo,
        'descricao': descricao,
        'tipo': tipo,
        # 'pdf': pdf
    }

    return detalhes


if __name__ == "__main__":
    lista_materias = []
    # 24 página
    for i in range(25):
        materias = get_soup(
            f'https://www.camarajaguaribara.ce.gov.br/materias.php?pagina={i}')
        m_principal = materias.find('div', {'id': 'Noticia'})

        m_posts = m_principal.find_all('div', {'class': 'cbp_tmlabel'})
        for m in m_posts:
            url = m.find_all('a')[0].attrs['href']

            try:
                url_pdf = m.find_all('a')[3].attrs['href']
                if url_pdf.endswith('.pdf'):
                    pdf = get_pdf(
                        f'https://www.camarajaguaribara.ce.gov.br/{url_pdf}')
            except IndexError:
                pdf = ''

            m_detalhes = get_detalhes(url)
            m_detalhes['pdf'] = pdf
            lista_materias.append(m_detalhes)

    # print(lista_materias)
    write_json(lista_materias, 'materias')
