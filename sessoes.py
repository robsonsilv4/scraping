from materias import get_soup, get_pdf, write_json

if __name__ == "__main__":
    lista_sessoes = []
    for i in range(6):
        sessoes = get_soup(
            f'https://www.camarajaguaribara.ce.gov.br/sessao.php?pagina={i}')

        # Div com os "posts"
        m_principal = sessoes.find('div', {'id': 'Noticia'})

        # "Posts"
        m_posts = m_principal.find_all('div', {'class': 'cbp_tmlabel'})

        for m in m_posts:
            # 16
            # 17 esp
            titulo = m.find_all('h2')[0].get_text()[:-16]
            data = m.find_all('h2')[0].span.get_text()  # len(data) == 16

            descricao = m.find('p').get_text().strip()
            ata = ''
            pauta = ''

            # Quando tem ata, ela vem primeiro
            try:
                url_ata = m.find_all('a')[6].attrs['href']
                ata = get_pdf(url_ata, 'sessoes')
            except (KeyError, IndexError):
                ata = ''

            # Alguns posts não tem pautas
            # 2. pode não ter nenhum dos dois tbm
            try:
                url_pauta = m.find_all('a')[3].attrs['href']
                if 'pauta' in url_pauta:
                    pauta = get_pdf(url_pauta, 'sessoes')
                else:
                    ata = url_pauta
            except (KeyError, IndexError):
                pauta = ''

            sessao = {
                'titulo': titulo,
                'data': data,
                'descricao': descricao,
                'ata': ata,
                'pauta': pauta
            }

            lista_sessoes.append(sessao)

    write_json(lista_sessoes, sessoes)
